WARNING! This repository is not longer active and each container image has been moved to its repository under this Gitlab group: https://gitlab.com/CentOS/automotive/container-images

# CentOS Automotive Container Images

This repostory contains all the required files to build linux container images
that are used in AutoSD OS images.

Images are pushed to `quay.io/centos-sig-automotive/$FOLDER_NAME:latest`.

## License

[MIT](./LICENSE)
