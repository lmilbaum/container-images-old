# Images

* `autosd`: base development AutoSD image which contains Bluechi, Systemd and QM;
* `autosd-buildbox`: base rpmbuild environment to build AutoSD RPMs, toolbox friendly; 
* `eclipse-chariott`: images for the Eclipe chariott project;
* `eclipse-kuksa`: images for the Eclipse Kuksa Val project.
* `eclipse-mosquitto`: images for the [Eclipse
  Mosquitto](https://github.com/eclipse/mosquitto) project;
