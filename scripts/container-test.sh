#!/bin/bash
LIB_DIR="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )/lib"

source $LIB_DIR/git.sh
source $LIB_DIR/log.sh
source $LIB_DIR/container.sh

if [ -z "${CI_MERGE_REQUEST_DIFF_BASE_SHA+x}" ]; then
    REFS="$CI_MERGE_REQUEST_TARGET_BRANCH_NAME"
else
    REFS=$CI_MERGE_REQUEST_DIFF_BASE_SHA
fi

log::info "Using refs: $REFS"

if [ -z "${PATHS+x}" ]; then
    PATHS=$(git::diff $REFS | grep images | grep -oE "^images/[a-zA-Z_\-]+" | uniq)
fi

log::info "Identified changes:"
for path in $PATHS; do
    echo -e "\t$path"
done

buildah images

log::info "Tested images:"
for path in $PATHS; do
    echo -e "\t${IMG_REG}/${IMG_ORG}/$(basename $path):${IMG_TAG}"
done
