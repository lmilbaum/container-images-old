#!/bin/bash
LIB_DIR="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )/lib"

source $LIB_DIR/git.sh
source $LIB_DIR/log.sh
source $LIB_DIR/container.sh

container::login "${REG_URL}" "${REG_USERNAME}" "${REG_PASSWORD}"

for path in $(buildah images | grep -e 'quay.io/centos-sig-automotive/*' | awk '{print $1}'); do
    log::info "Pushing $path..."
    container::push $path
    log::info "$path pushed."
done

container::logout "${REG_URL}"

log::info "Published images:"
for path in $PATHS; do
    echo -e "\t${IMG_REG}/${IMG_ORG}/$(basename $path):${IMG_TAG}"
done
