log::info() {
    echo "==> [INFO] $@"
}

log::error() {
    echo "==> [ERROR] $@" 1>&2;
}
